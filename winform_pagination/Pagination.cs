﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform_pagination
{
    public partial class Pagination : UserControl
    {
        public Pagination()
        {
            InitializeComponent();

            TotalCount = 0;
            PageSize = 10;
            CurrentPage = 1;
        }

        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPage
        {
            get
            {
                return TotalCount % PageSize == 0 ? TotalCount / PageSize : TotalCount / PageSize + 1;
            }
        }

        public int TotalCount { get; set; }

        private void RefreshLabels()
        {
            label1.Text = $"TotalPage={TotalPage},TotalCount={TotalCount},CurrentPage={CurrentPage},PageSize={PageSize}";
        }

        public event EventHandler<PaginationEventArgs> Paging;

        private void RefreshPaging()
        {
            RefreshLabels();
            Paging?.Invoke(this, new PaginationEventArgs { Skip = (CurrentPage - 1) * PageSize, Take = PageSize });
        }

        public void RunFirstTimePaging()
        {
            CurrentPage = 1;
            RefreshPaging();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            CurrentPage = 1;
            RefreshPaging();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (CurrentPage < TotalPage)
            {
                CurrentPage = CurrentPage + 1;
            }
            RefreshPaging();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (CurrentPage > 1)
            {
                CurrentPage = CurrentPage - 1;
            }
            RefreshPaging();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            CurrentPage = TotalPage;
            RefreshPaging();
        }
    }
}
