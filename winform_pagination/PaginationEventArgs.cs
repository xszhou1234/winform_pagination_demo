﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform_pagination
{
    public class PaginationEventArgs:EventArgs
    {
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}
