namespace winform_pagination
{
    public partial class Form1 : Form
    {
        private List<Person> data;
        public Form1()
        {
            InitializeComponent();
            //准备好模拟用的数据源
            data = new List<Person>();
            int datacount = Random.Shared.Next(10, 100000);
            for (int i = 0; i < datacount; i++)
            {
                data.Add(new Person
                {
                    Id = i + 1,
                    Name = $"Person {i + 1}",
                    Age = Random.Shared.Next(10, 100)
                });
            }

            //设置分页控件
            pagination1.PageSize = 20;
            pagination1.TotalCount = data.Count;
            pagination1.Paging += Pagination1_Paging;

            pagination1.RunFirstTimePaging();
        }

        private void Pagination1_Paging(object? sender, PaginationEventArgs e)
        {

            //每次只加载需要的数据
            var filter_data = data.OrderBy(i => i.Id).Skip(e.Skip).Take(e.Take).ToList();

            //datagridview只负责显示
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = filter_data;
            dataGridView1.Update();


            listBox1.DisplayMember = "Name";
            this.listBox1.DataSource= filter_data;
            this.listBox1.Update();
        }

    }
}